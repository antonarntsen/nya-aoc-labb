import pytest
from main import present_area
from main import ribbon

def test_present_area():
    assert present_area(["2x3x4"]) == 58
    assert present_area(["1x1x10"]) == 43
    assert present_area(["1x10x1"]) == 43

def test_ribbon():
    assert ribbon(["2x3x4"]) == 34
    assert ribbon(["1x1x10"]) == 14
    assert ribbon(["1x10x1"]) == 14