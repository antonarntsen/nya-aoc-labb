


def area_list():
    file = open("day2/inputday2.txt")
    area = file.readlines()
    file.close()
    return area

input = area_list()

def present_area(inp):
    total_area = 0
    for box in inp:
        l, w, h = sorted(map(int, box.split("x")))
        area = (2 * l * w) + (2 * l * h) + (2 * w * h)
        slack = l * w
        total_area += (area + slack)
        print(total_area)
    return total_area

def ribbon(inp):
    ribbon_length = 0
    for box in inp:
        l, w, h = sorted(map(int, box.split("x")))
        ribbon_present = l + l + w + w
        ribbon_bow = l * w * h
        ribbon_length += (ribbon_present + ribbon_bow)
        print(ribbon_length)
    return ribbon_length
        

present_area(input)
ribbon(input)



