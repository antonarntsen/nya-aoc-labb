def floorcount(inp):
    floor = 0

    for character in inp:
        if character == "(":
            floor += 1
        elif character == ")":
            floor -=1
    return floor

def check_if_basement(inp):
    charnum = 0
    floor = 0

    for character in inp:
        charnum += 1

        if character == "(":
            floor += 1

        elif character == ")":
            floor -=1
        
        if floor == -1:
            break
    return charnum
        
with open("day1/inputday1_2015.txt") as f:
    inp = f.read()
    print(floorcount(inp))
    print(check_if_basement(inp))