import pytest
from main import check_if_basement
from main import floorcount

def test_floorcount():
    assert floorcount("(())") == 0
    assert floorcount("()()") == 0
    assert floorcount("(((") == 3
    assert floorcount("(()(()(") == 3
    assert floorcount(")())())") == -3

def test_check_if_basement():
    assert check_if_basement(")())())") == 1
    assert check_if_basement("()()()())") == 9
    assert check_if_basement("()()())())") == 7
    assert check_if_basement("()())") == 5
    assert check_if_basement("()(((((((()))))))))") == 19